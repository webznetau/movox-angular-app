export interface AbnFinderApiResponse { 
    Abn: string;
    AbnStatus: string;
    AddressData: string;
    AddressPostcode: string;
    AddressState: string;
    BusinessName: string;
    EntityName: string;
    EntityTypeCode: string;
    EntityTypeName: string;
    Gst: string;
    Message: string;    
}
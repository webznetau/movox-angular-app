import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient, HttpHeaders} from '@angular/common/http';

//Services
import {AppSettingsService} from '../../services/app-settings.service';

//Interfaces
//import {AddressFinderApiResponse} from './address-finder-api-response';

const httpOptions = {
    headers: new HttpHeaders({
        'Content-Type':  'application/text'
    })
};

@Injectable({
  providedIn: 'root'
})
export class AbnFinderService {

   apiUrl: string;
    
    constructor(
        private settings: AppSettingsService,
        private http: HttpClient
    ){ 
        this.apiUrl = this.settings.get('abnFinderApiUrl');
    }
    
    find(abn: number): Observable<string> {       
        return this.http.get(`${this.apiUrl}${abn}`,  {responseType: 'text'});
    }
}

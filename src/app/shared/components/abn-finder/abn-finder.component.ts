import {Component, OnInit, EventEmitter, Output, Input} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {map, startWith, debounceTime} from 'rxjs/operators';

//Services
import {AbnFinderService} from './abn-finder.service';

@Component({
  selector: 'app-abn-finder',
  templateUrl: './abn-finder.component.html',
  styleUrls: ['./abn-finder.component.css']
})
export class AbnFinderComponent implements OnInit {
    @Output() result: EventEmitter<any> = new EventEmitter();
    @Input() value: string = '';
    
    abn = new FormControl('', [
        Validators.required,
//        Validators.min(0),
//        Validators.minLength(11),
//        Validators.maxLength(11)
    ]);
    data: any = null;
    inProgress: boolean;
    isValid: boolean = true;
    isActive: boolean;
    isFail: boolean = false;
    
    constructor(
        private abnFinder: AbnFinderService
    ) { }

    ngOnInit() {
        this.data = this.abn.valueChanges.pipe(
            debounceTime(1000),
            startWith(),
            map(val => {
                if (this.value !== null){
                    this.find(this.value);
                }
                else{
                    if(val!==''){
                        this.find(val);
                    }
                }
            })  
        ).subscribe();
        
        this.abn.setValue(this.value);
    }
    
    find(abn): any{
        if(abn){
            this.inProgress = true;
            
            this.abnFinder.find(abn).subscribe(
                response => {
                    this.inProgress = false;
                    response = response.replace('callback(', '');
                    response = response.replace('})', '}');
                    this.data = JSON.parse(response);
                    
                    this.isValid = this.data.Abn == '' ? false : true;
                    
                    if (!this.isValid){
                        this.abn.setErrors({'': true});
                        this.abn.markAsTouched();
                    }
                    
                    this.isActive = this.data.AbnStatus == 'Active' ? true : false;
                    this.isFail = false;
                    
                    if (this.isActive && this.isValid){
                        this.onSuccess();
                    }
                    else{
                        this.onFail();
                    }
                },
                () => {
                    this.inProgress = false;
                    this.isFail = true;
                }
            );
        }
    }
    
    onSuccess(): void{
        this.result.emit(this.data);
    }
    
    onFail(): void{
        this.result.emit(null);
    }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AbnFinderComponent } from './abn-finder.component';

describe('AbnFinderComponent', () => {
  let component: AbnFinderComponent;
  let fixture: ComponentFixture<AbnFinderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AbnFinderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AbnFinderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

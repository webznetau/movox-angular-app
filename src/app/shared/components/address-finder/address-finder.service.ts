import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {HttpClient} from '@angular/common/http';

//Services
import {AppSettingsService} from '../../services/app-settings.service';

//Interfaces
import {AddressFinderApiResponse, AddressFinderApiResponseAddressDetails} from './address-finder-api-response';

@Injectable({
    providedIn: 'root'
})
export class AddressFinderService {
    
    apiUrl: string;
    apiDetailsUrl: string;
    
    constructor(
        private settings: AppSettingsService,
        private http: HttpClient
    ){ 
        this.apiUrl = this.settings.get('addressFinderApiUrl');
        this.apiDetailsUrl = this.settings.get('addressFinderApiDetailsUrl');
    }
    
    find(address: string): Observable<AddressFinderApiResponse> {        
        return this.http.get<AddressFinderApiResponse>(`${this.apiUrl}${address}`);
    }

    details(id: number): Observable<AddressFinderApiResponseAddressDetails> {
        return this.http.get<AddressFinderApiResponseAddressDetails>(`${this.apiDetailsUrl}${id}`);
    }
}

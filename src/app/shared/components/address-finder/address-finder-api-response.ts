export interface AddressFinderApiResponse {
   completions: AddressFinderApiResponseAddress[];
   paid: boolean;
   demo: boolean;
}

export interface AddressFinderApiResponseAddress{
    full_address: string;
    id: string;
    v: number;
    highlighted_full_address?: string;
}

export interface AddressFinderApiResponseAddressDetails{
    full_address: string;
    id: string;
    unit_identifier: string;
    street_number_1: number;
    street_number_2: number;
    street_name: string;
    street_type: string;
    locality_name: string;
    state_territory: string;
    postcode: string;
    box_identifier: string;
    box_type: string;
    success: boolean;
}
import {Component, OnInit, EventEmitter, Output, Input} from '@angular/core';
import {FormControl} from '@angular/forms';
import {Observable} from 'rxjs';
import {map, startWith, debounceTime, distinctUntilChanged, switchMap, tap} from 'rxjs/operators';

//Services
import {AddressFinderService} from './address-finder.service';

//Interfaces
import {AddressFinderApiResponseAddress, AddressFinderApiResponseAddressDetails} from './address-finder-api-response';

export interface Address {
    address: string;
}

@Component({
    selector: 'app-address-finder',
    templateUrl: './address-finder.component.html',
    styleUrls: ['./address-finder.css']
})
export class AddressFinderComponent implements OnInit {
    @Output() onAddressSelect: EventEmitter<AddressFinderApiResponseAddressDetails> = new EventEmitter();
    @Output() onCantFindAddress: EventEmitter<boolean> = new EventEmitter();
    @Input() placeholder: string = 'Address';

    filteredAddresses;
    field = new FormControl();
    inProgress: boolean = false;
    inProgressDetails: boolean = false;
    cantFind: boolean = false;
    selectedVal: string;

    constructor(
        private finder: AddressFinderService
    ) { }

    ngOnInit() {
        this.filteredAddresses = this.field.valueChanges.pipe(
            startWith(null),
            debounceTime(500),
            distinctUntilChanged(),
            switchMap(val => {
                if(val == ''){
                    this.inProgress = false;
                }
                return (val && val !== this.selectedVal) ? this.filter(val || '') : [];
            })
        );
    }

    filter(address: string): Observable<AddressFinderApiResponseAddress[]> {
        return this.finder.find(address)
        .pipe(
            tap(
                response => {
                    if(this.inProgress && !response.completions.length){
                        this.onCantFindAddress.emit();
                    }
                    this.inProgress = false;
                },
                _ => {
                    this.inProgress = false;
                }
            ),
            map(response => response.completions.filter(option => {
                this.cantFind = false;
                return option.full_address.toLowerCase().indexOf(address.toLowerCase()) === 0
            })),
        )
    }

    displayFn(address?: string): string | undefined {
        return address ? address : undefined;
    }

    onSelect(event, address: string, id: number): void{
        this.selectedVal = address;
        this.inProgressDetails = true;

        return this.getDetails(id);
    }

    getDetails(id: number): void{
        this.finder.details(id).subscribe(
            details => {
                if(details.success){
                    this.onAddressSelect.emit(details);
                    this.field.reset();
                }
                this.inProgressDetails = false;
            },
            () => {
                this.inProgressDetails = false;
            }
        );
    }

    onChange(event: any): void{

    }

    onKeyup(event: KeyboardEvent){
        this.inProgress = true;
    }

}

import { TestBed } from '@angular/core/testing';

import { AddressFinderService } from './address-finder.service';

describe('AddressFinderService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: AddressFinderService = TestBed.get(AddressFinderService);
    expect(service).toBeTruthy();
  });
});

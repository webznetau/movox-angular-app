import {Component, OnInit, Input, Output,EventEmitter} from '@angular/core';
import {FormControl, FormGroup, Validators } from '@angular/forms';
import {map, startWith, debounceTime} from 'rxjs/operators';
import {CreditCardData} from './credit-card-data';

@Component({
  selector: 'app-credit-card-form',
  templateUrl: './credit-card-form.component.html',
  styleUrls: ['./credit-card-form.component.css']
})
export class CreditCardFormComponent implements OnInit {
    @Input() formData: CreditCardData;
    @Output() formObject: EventEmitter<any> = new EventEmitter();
    @Output() formDataChange: EventEmitter<any> = new EventEmitter();

    form: FormGroup;

    constructor() {
    }

    ngOnInit() {
        
        this.form = new FormGroup({
            'credit_card_holder_name': new FormControl(this.formData.credit_card_holder_name, [
                Validators.required
            ]),
            'credit_card_number': new FormControl(this.formData.credit_card_number, [
                Validators.required,
                Validators.minLength(16),
                Validators.maxLength(16),
            ]),
            'credit_card_cvc': new FormControl(this.formData.credit_card_cvc, [
                Validators.required,
                Validators.minLength(3),
                Validators.maxLength(3),
            ]),
            'credit_card_valid_to': new FormControl(this.formData.credit_card_valid_to, [
                Validators.required,
                Validators.minLength(5),
                Validators.maxLength(5),
            ])
        });
    
        this.formObject.emit(this.form);
        
        this.form.valueChanges.pipe(
            debounceTime(1000),
            startWith(),
            map(() => {
                this.formDataChange.emit();
            })  
        ).subscribe();
    }

}

export interface CreditCardData{
    credit_card_holder_name: string;
    credit_card_number: number;
    credit_card_cvc: number;
    credit_card_valid_to: string;
}
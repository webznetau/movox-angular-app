import {Injectable} from '@angular/core';
import {ToastrService} from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

    constructor( 
        private toastr : ToastrService
    ) {}
    
    private toastrDefaults : any = {
        positionClass	: 'toast-bottom-full-width'
   };
  
    filter( message : string ) {
        return message.replace(/<br ?\/?>/g, '\n' ); 
   }
    
    overWriteDefaultOptions( newOptions : any ) : object {      
        return Object.assign( this.toastrDefaults, newOptions );        
   }
    
    error( message : string, title : string = null, options : object = {} ) : void {  
        this.showAlert( 'error', this.filter( message ), title, options );                
   }
    
    success( message : string, title : string = null, options : object = {} ) : void {     
        this.showAlert( 'success', this.filter( message ), title, options );                
   }
    
    info( message : string, title : string = null, options : object = {} ) : void {     
        this.showAlert( 'info', this.filter( message ), title, options );                
   }
    
    warning( message : string, title : string = null, options : object = {} ) : void {     
        this.showAlert( 'warning', this.filter( message ), title, options );                
   }
    
    showAlert( alertType : string, message : string, title : string, options : object) : void {
        
        options = this.overWriteDefaultOptions( options );
        
        switch( alertType ) {
            case 'error':
                this.toastr.error( this.filter( message ), title, options );
                break;
            case 'success':
                this.toastr.success( this.filter( message ), title, options );
                break;
            case 'warning':
                this.toastr.warning( this.filter( message ), title, options );
                break;
            case 'info':
                this.toastr.info( this.filter( message ), title, options );
                break;
            default:
                this.toastr.info( this.filter( message ), title, options );
       }
        
   }
    
}

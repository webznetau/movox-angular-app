import {CanActivate, Router} from '@angular/router';
import {Injectable} from '@angular/core';
import {UserService} from '../../../user/shared/services/user.service';

@Injectable()
export class LoggedInRouteGuard implements CanActivate {

    constructor( private userService: UserService, private router: Router ) {}

    canActivate() {
        if( this.userService.isLogged() ) {
            return true;
       }
        else {
            this.router.navigate(['user/signin']);
            return false;
       }
   }
}



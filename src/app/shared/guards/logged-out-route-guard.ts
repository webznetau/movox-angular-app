import {CanActivate} from '@angular/router';
import {Injectable} from '@angular/core';
import {UserService} from '../../../user/shared/services/user.service';

@Injectable()
export class LoggedOutRouteGuard implements CanActivate {

    constructor( private userService: UserService ) {}

    canActivate() {
        return !this.userService.isLogged()
   }
}



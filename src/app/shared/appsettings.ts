export class AppSettings {
    apiUrl: string = 'https://example-api-domain.com.au';
    addressFinderApiUrl = 'https://api.addressfinder.io/api/au/address?key=TRL3DM9KQBFUC8PNXH4V&format=json&strict=0&q=';
    addressFinderApiDetailsUrl = 'https://api.addressfinder.io/api/au/address/info?key=TRL3DM9KQBFUC8PNXH4V&format=json&id=';
    //abnFinderApiUrl = 'https://abr.business.gov.au/json/AbnDetails.aspx?guid=29c87326-cc00-4606-8685-4d5baf8e4527&abn=';
    abnFinderApiUrl = 'http://api.movox.webzdev.net.au/abn-finder.php?abn=';
    contactNumber: string = '1800 100 800';
    contactEmail: string = 'info@movox.com.au';
}

//secret=PNGJKVCTY4AQ8WLEFMR7&
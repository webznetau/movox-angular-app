import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {ToastrModule} from 'ngx-toastr';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DateFormat} from './core/date-format';
import {DateAdapter} from '@angular/material';

//Font awesome
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';
import {library} from '@fortawesome/fontawesome-svg-core';
import {fas} from '@fortawesome/free-solid-svg-icons';
import {far} from '@fortawesome/free-regular-svg-icons';
library.add(fas, far);

//Material
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatButtonModule} from '@angular/material/button';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';

//Components
import {AppComponent} from './app.component';
import {PageNotFoundComponent} from './core/components/page-not-found/page-not-found.component';
import {AccessDeniedComponent} from './core/components/access-denied/access-denied.component';
import {DashboardComponent} from './components/dashboard/dashboard.component';

@NgModule({
    declarations: [
        AppComponent,
        PageNotFoundComponent,
        AccessDeniedComponent,
        DashboardComponent
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        ToastrModule.forRoot(),
        HttpClientModule,
        AppRoutingModule,
        FontAwesomeModule,
        FormsModule,
        ReactiveFormsModule,
        //Material
        MatMenuModule,
        MatIconModule,
        MatButtonModule,
        MatFormFieldModule,
        MatProgressSpinnerModule
    ],
    providers: [
      { provide: DateAdapter, useClass: DateFormat }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
    constructor(private dateAdapter:DateAdapter<Date>) {
        dateAdapter.setLocale('en-au');
    }
}

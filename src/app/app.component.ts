import {Component} from '@angular/core';
import {AppSettingsService} from '../app/shared/services/app-settings.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'movox';
    
    constructor(
        public settings: AppSettingsService
    ) { }
}

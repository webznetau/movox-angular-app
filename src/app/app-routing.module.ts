import {NgModule}             from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

//Guards
import {LoggedInRouteGuard} from './shared/guards/logged-in-route-guard';
import {LoggedOutRouteGuard} from './shared/guards/logged-out-route-guard';

//Components
import {DashboardComponent} from './components/dashboard/dashboard.component';
import {PageNotFoundComponent} from './core/components/page-not-found/page-not-found.component';

export const appRoutingProviders: any[] = [];

const routes: Routes = [
    {
        path: '',
        component: DashboardComponent,
        canActivate: [LoggedInRouteGuard],
    },
    {
        path: 'user',
        loadChildren: '../user/user.module#UserModule'
    },
   
   {path: '**', component: PageNotFoundComponent}
    
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
    providers: [LoggedInRouteGuard, LoggedOutRouteGuard]
})

export class AppRoutingModule {}
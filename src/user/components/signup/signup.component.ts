import {Component, OnInit, OnDestroy, Inject} from '@angular/core';
import {FormControl, FormGroup, Validators } from '@angular/forms';
import {Router, ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';
import {map, debounceTime} from 'rxjs/operators';
import {MatDialog, MatDialogRef} from '@angular/material';

//Interfaces
import {SignupData, SignupDataAddress, SignupDataDirector} from '../../interfaces/signup-data';
import {AbnFinderApiResponse} from '../../../app/shared/components/abn-finder/abn-finder-api-response';
import {AddressFinderApiResponseAddressDetails} from '../../../app/shared/components/address-finder/address-finder-api-response';

//Services
import {AlertService} from '../../../app/shared/services/alert.service';

const accountTypes = {
    'sole_trader': "Sole Trader",
    'personal': "Personal",
    'company': "Company"
};

const nameTitles = {
    MR: 'Mr',
    MS: 'Ms',
    MISS: 'Miss',
    MRS: 'Mrs',
    CAPT: 'Captain',
    DR: 'Doctor',
    PROF: 'Professor',
    REV: 'Reverend'
}

const banksNames = {
    NAB: 'NAB',
    ANZ: 'ANZ',
    WESTPACK: 'WestPack',
    COMMBANK: 'COMMBANK',
    OTHER: 'Other'
}

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit, OnDestroy {

    private curStep: string|number = 0;
    form: FormGroup;
    addressForm: FormGroup;
    correspondenceAddressForm: FormGroup;
    formControl: FormControl;
    stepTitle: string;
    addressFinderPlaceholder: string;
    accountTypes = accountTypes;
    abnSuccess: boolean;
    abnTempData: any;
    urlParams: Subscription;
    nameTitles: any = nameTitles;
    banksNames: any = banksNames;
    creditCardFormObject: FormGroup;
    creditCardValid: boolean = null;
    inProgress: boolean = false;
    hidePassword: boolean = true;
    userIsDirector: boolean;

    signupData: SignupData = {
        address: null,
        correspondence_address: null,
        correspondence_address_same: true,
        accept_customer_terms: null,
        email: null,
        password: null,
        account_type: 'company',
        name_title: null,
        first_name: null,
        last_name: null,
        contact_number: null,
        mobile_number: null,
        date_of_birth: null,
        abn: null,
        abn_legal_name: null,
        abn_entity_type: null,
        abn_trading_as: null,
        payment_method: null,
        accept_payment_auto_debit_credit_card: null,
        accept_payment_auto_debit_credit_card_privacy: null,
        accept_payment_direct_debit: null,
        accept_payment_direct_debit_privacy: null,
        directors: []
    };

    constructor(
        private router: Router,
        private route: ActivatedRoute,
        public alert: AlertService,
        public dialog: MatDialog
    ) {

    }

    ngOnInit(){
        
        let signupData = sessionStorage.getItem('signupData');

        if(signupData){
            this.signupData = JSON.parse(signupData);
        }

        let stepUrl = this.route.snapshot.params['step'];

        if(stepUrl){
            this.urlParams = this.route.params.subscribe(params => {
                this.curStep = params.step;
                this.userIsDirector = sessionStorage.getItem('userIsDirector') == '1' ? true : false;
                this.stepsRouter();
            });
        }
        else{
            let stepSession = sessionStorage.getItem('signupCurStep');

            if(stepSession){
                this.curStep = stepSession;
            }
            else{
                this.curStep = 0;
            }
            this.stepsRouter();
        }
    }

    ngOnDestroy(){
        if(this.urlParams){
            this.urlParams.unsubscribe();
        }
    }

//    stepAcknowledgements(): void{
//        this.form = new FormGroup({
//            accept_customer_terms: new FormControl(this.signupData.accept_customer_terms, [
//                Validators.requiredTrue
//            ])
//        });
//
//        this.form.valueChanges.pipe(
//            map(val => {
//                this.assignData(val);
//            })
//        ).subscribe();
//
//        this.stepTitle = 'Acknowledgement';
//        this.setCurStep('acknowledgements');
//    }

    stepAuthData(): void{
        this.form = new FormGroup({
            email: new FormControl(this.signupData.email, [
                Validators.required,
                Validators.email
            ]),
            password: new FormControl(this.signupData.password, [
                Validators.required,
                Validators.minLength(8)
            ]),
            accept_customer_terms: new FormControl(this.signupData.accept_customer_terms, [
                Validators.requiredTrue
            ])
        });

        this.form.valueChanges.pipe(
            debounceTime(500),
            map(val => {
                this.assignData(val);
            })
        ).subscribe();

        this.setCurStep('auth_data');
        this.stepTitle = 'Customer Account Application';
    }

    stepClientData(): void{
        this.form = new FormGroup({
            name_title: new FormControl(this.signupData.name_title, [
                Validators.required
            ]),
            first_name: new FormControl(this.signupData.first_name, [
                Validators.required
            ]),
            last_name: new FormControl(this.signupData.last_name, [
                Validators.required
            ]),
            date_of_birth: new FormControl(this.signupData.date_of_birth, [
                Validators.required
            ])
        });
        
        this.formControl = new FormControl(this.userIsDirector);

        if(this.signupData.account_type == 'company'){
            this.form.addControl('mobile_number', new FormControl(this.signupData.mobile_number, [
                Validators.required
            ]));
        }
        else{
            this.form.addControl('contact_number', new FormControl(this.signupData.contact_number, [
                Validators.required
            ]));
        }

        this.form.valueChanges.pipe(
            debounceTime(500),
            map(val => {
                this.assignData(val);
            })
        ).subscribe();        
                
        this.formControl.valueChanges.pipe(
            map(val => {
                if(val){
                    sessionStorage.setItem('userIsDirector', '1');
                    this.addDirector(this.form, false);
                }
                else{
                    sessionStorage.setItem('userIsDirector', '0');
                    this.deleteDirector(this.form.value);
                }
            })
        ).subscribe();

        this.setCurStep('client_data');
        this.stepTitle = 'Details of the authorised account holder';
    }

    stepIdVerification(): void{
        this.setCurStep('id_verification');
        this.stepTitle = 'ID verification';
    }

    stepDirectorsDetails(): void{
        this.form = new FormGroup({
            name_title: new FormControl('', [
                Validators.required
            ]),
            first_name: new FormControl('', [
                Validators.required
            ]),
            last_name: new FormControl('', [
                Validators.required
            ]),
            date_of_birth: new FormControl('', [
                Validators.required
            ]),
            mobile_number: new FormControl('', [
                Validators.required
            ])
        });

        this.setCurStep('directors_details');
        this.stepTitle = 'Director details';
    }

    addDirector(form: FormGroup, resetForm: boolean = true){
        let directors: SignupDataDirector[] = this.signupData.directors;
        
        if(directors == null){
            directors = [];
        }
        
        directors.push(form.value);
        this.assignData({directors: directors});
        
        if(resetForm){
            this.form.reset();
        }
    }

    deleteDirector(director: SignupDataDirector): void{
        let directors: SignupDataDirector[] = this.signupData.directors;
        directors.splice(directors.indexOf(director), 1);
        this.assignData({directors: directors});
    }

    stepAddress(): void{

        this.formControl = new FormControl(this.signupData.correspondence_address_same);

        this.addressForm = new FormGroup({
            po_box: new FormControl('', []),

            unit_number: new FormControl('', []),

            street_number: new FormControl('', [
                Validators.required
            ]),

            street_name: new FormControl('', [
                Validators.required
            ]),

            street_type: new FormControl('', [
                Validators.required
            ]),

            suburb: new FormControl('', [
                Validators.required
            ]),

            state: new FormControl('', [
                Validators.required
            ]),

            postcode: new FormControl('', [
                Validators.required
            ])
        });

        if(this.signupData.address){
            this.addressForm.setValue(this.signupData.address);
        }

        this.correspondenceAddressForm = new FormGroup({
            po_box: new FormControl('', []),

            unit_number: new FormControl('', []),

            street_number: new FormControl('', [
                Validators.required
            ]),

            street_name: new FormControl('', [
                Validators.required
            ]),

            street_type: new FormControl('', [
                Validators.required
            ]),

            suburb: new FormControl('', [
                Validators.required
            ]),

            state: new FormControl('', [
                Validators.required
            ]),

            postcode: new FormControl('', [
                Validators.required
            ])
        });

        if(this.signupData.correspondence_address){
            this.correspondenceAddressForm.setValue(this.signupData.correspondence_address);
        }

        this.addressForm.valueChanges.pipe(
            map(val => {
                if(this.addressForm.valid){
                    this.assignData({address: val});
                    
                    if(this.signupData.correspondence_address_same){
                        this.assignData({correspondence_address: val});
                    }
                }
            })
        ).subscribe();
        
        this.formControl.valueChanges.pipe(
            map(val => {
                this.assignData({correspondence_address_same: val});
                
                if(val){
                    this.correspondenceAddressForm.setValue(this.addressForm.value);
                }
                else{
                    this.assignData({correspondence_address: null});
                    this.correspondenceAddressForm.reset();
                }
            })
        ).subscribe();

        this.correspondenceAddressForm.valueChanges.pipe(
            map(val => {
                if(this.correspondenceAddressForm.valid){
                    this.assignData({correspondence_address: val});
                }
            })
        ).subscribe();

        if (!this.signupData.correspondence_address_same){
            this.addressFinderPlaceholder = 'Correspondence Address';
        }

        this.setCurStep('address');
        this.stepTitle = 'Address details';
    }

    stepAbn(): void{
        this.setCurStep('abn');
        this.stepTitle = 'Australian business number';

        //20 980 110 294  - example inavtive ABN
    }

    stepPaymentMethod(): void{
        this.setCurStep('payment_method');
        this.stepTitle = 'Monthly bill payment method';
    }

    stepPaymentMethodAutoDebitCreditCard(): void{
        this.form = new FormGroup({
            accept_payment_auto_debit_credit_card: new FormControl(this.signupData.accept_payment_auto_debit_credit_card, [
                Validators.requiredTrue
            ]),
            accept_payment_auto_debit_credit_card_privacy: new FormControl(this.signupData.accept_payment_auto_debit_credit_card_privacy, [
                Validators.requiredTrue
            ])
        });

        this.form.valueChanges.pipe(
            map(val => {
                this.assignData(val);
            })
        ).subscribe();

        this.setCurStep('payment_method_auto_debit_credit_card');
        this.stepTitle = 'Direct debit request (DDR)';
    }

    stepPaymentMethodDirectDebit(): void{
        this.form = new FormGroup({
            direct_debit_bank_name: new FormControl(this.signupData.direct_debit_bank_name, [
                Validators.required
            ]),
            direct_debit_bsb: new FormControl(this.signupData.direct_debit_bsb, [
                Validators.required
            ]),
            direct_debit_account_number: new FormControl(this.signupData.direct_debit_account_number, [
                Validators.required
            ]),
            direct_debit_account_name: new FormControl(this.signupData.direct_debit_account_name, [
                Validators.required
            ]),
            accept_payment_direct_debit: new FormControl(this.signupData.accept_payment_direct_debit, [
                Validators.requiredTrue
            ]),
            accept_payment_direct_debit_privacy: new FormControl(this.signupData.accept_payment_direct_debit_privacy, [
                Validators.requiredTrue
            ])
        });

        this.form.valueChanges.pipe(
            map(val => {
                this.assignData(val);
            })
        ).subscribe();

        this.setCurStep('payment_method_direct_debit');
        this.stepTitle = 'Direct debit request (DDR)';
    }

    stepThankYou(): void{
        this.clearSignupData();
        this.stepTitle = 'Welcome to MOVOX';
    }

    signUp(): void{
        //Send signup data to api
        this.inProgress = true;

        setTimeout(() => {
            this.inProgress = false;
            this.navigateToStep('thank_you');
        }, 2000);
    }

    isStep(step: number|string){
        return step == this.curStep ? true : false;
    }

    onAddressSelect(address_details: AddressFinderApiResponseAddressDetails): void{
        let details = this.convertAddressFinderToAddress(address_details)
        this.assignData({address: details});
        this.addressForm.setValue(details);
        
        //If corresponence address same is checked - set same data into correspondence address form
        if(this.formControl.value){
            this.correspondenceAddressForm.setValue(this.addressForm.value);
        }
    }

    onCorrespondenceAddressSelect(address_details: AddressFinderApiResponseAddressDetails): void{
        let details = this.convertAddressFinderToAddress(address_details)
        this.assignData({correspondence_address: details});
        this.correspondenceAddressForm.setValue(details);
    }

    private convertAddressFinderToAddress(data: AddressFinderApiResponseAddressDetails): SignupDataAddress{
        let _data: any = {
            po_box: data.box_identifier && data.box_type ? data.box_identifier + ' ' + data.box_type : '',
            unit_number: data.unit_identifier,
            street_number: data.street_number_1 + (data.street_number_2 ? ' ' + data.street_number_2 : ''),
            street_name: data.street_name,
            street_type: data.street_type,
            suburb: data.locality_name,
            state: data.state_territory,
            postcode: data.postcode
        };

        return _data;
    }

    clearSignupData(): void{
        for(let field in this.signupData){
            this.signupData[field] = null;
        }

        sessionStorage.removeItem('signupCurStep');
        sessionStorage.removeItem('signupData');
    }

    abnResponse(data: AbnFinderApiResponse){
        if(data!==null){
            this.abnSuccess = true;

            this.abnTempData = {
                abn: data.Abn,
                abn_legal_name: data.EntityName,
                abn_entity_type: data.EntityTypeName,
                abn_trading_as: data.BusinessName
            };
        }
        else{
            this.abnSuccess = false;
        }
    }

    confirmAbn(data){
        this.assignData(this.abnTempData);
    }

    setPaymentMethod(method: string){
        this.assignData({payment_method: method});
    }

    onCreditCardFormObjectCreate(form: FormGroup): void{
        this.creditCardFormObject = form;
        this.checkCreditCard();
    }

    onCreditCardFormDataChange(): void{
        this.checkCreditCard();
        this.assignData(this.creditCardFormObject.value);
    }

    checkCreditCard(): void{
        this.creditCardValid = this.creditCardFormObject.valid ? true : false;
    }

    setAccountType(acc_type: string): void{
        this.signupData.account_type = acc_type;
        this.assignData({account_type: acc_type});
    }

    openAcknowledgementDialog(): void {
        const dialogRef = this.dialog.open(SignupAcknowledgementDialog, {

        });

        dialogRef.afterClosed().subscribe(result => {

        });
    }
    
    onCantFindAddress(): void{
        this.signupData.address = {};
    }

    nextStep(){
        this.stepsRouter('next');
    }

    prevStep(){
        this.stepsRouter('prev');
    }

    setCurStep(step: string|null): void{
        this.curStep = step;
        sessionStorage.setItem('signupCurStep', step);
    }

    navigateToStep(step: string): void{
        this.router.navigate([`/user/signup/${step}`]);

        switch(step){
//            case 'acknowledgements':
//                this.stepAcknowledgements();
//                break;

            case 'client_data':
                this.stepClientData();
                break;

            case 'id_verification':
                this.stepIdVerification();
                break;

            case 'directors_details':
                this.stepDirectorsDetails();
                break;

            case 'auth_data':
                this.stepAuthData();
                break;

            case 'address':
                this.stepAddress();
                break;

            case 'abn':
                this.stepAbn();
                break;

            case 'payment_method':
                this.stepPaymentMethod();
                break;

            case 'payment_method_auto_debit_credit_card':
                this.stepPaymentMethodAutoDebitCreditCard();
                break;

            case 'payment_method_direct_debit':
                this.stepPaymentMethodDirectDebit();
                break;

            case 'thank_you':
                this.stepThankYou();
                break;
        }
    }

    stepsRouter(direction?: string): void{
        if (this.curStep !== 0){
            switch (this.curStep){

                case 'acknowledgements':
                    switch(direction){
                        case 'next':
                            this.navigateToStep('auth_data');
                            break;
                        default:
                            this.navigateToStep('acknowledgements');
                            break;
                    }
                break;

                case 'auth_data':
                    switch(direction){
                        case 'next':
                            this.navigateToStep('client_data');
                            break;
                        case 'prev':
                            this.navigateToStep('acknowledgements');
                            break;
                        default:
                            this.navigateToStep('auth_data');
                            break;
                    }
                break;

                case 'client_data':
                    switch(direction){
                        case 'next':
                            this.navigateToStep('id_verification');
                            break;
                        case 'prev':
                            this.navigateToStep('auth_data');
                            break;
                        default:
                            this.navigateToStep('client_data');
                            break;
                    }
                break;

                case 'id_verification':
                    switch(direction){
                        case 'next':
                            if (this.signupData.account_type == 'company'){
                                this.navigateToStep('directors_details');
                            }
                            else{
                                this.navigateToStep('address');
                            }
                            break;
                        case 'prev':
                            this.navigateToStep('client_data');
                            break;
                        default:
                            this.navigateToStep('id_verification');
                            break;
                    }
                break;

                case 'directors_details':
                    switch(direction){
                        case 'next':
                            this.navigateToStep('address');
                            break;
                        case 'prev':
                            this.navigateToStep('id_verification');
                            break;
                        default:
                            this.navigateToStep('directors_details');
                            break;
                    }
                break;

                case 'address':
                    switch(direction){
                        case 'next':
                            this.navigateToStep('abn');
                            break;
                        case 'prev':
                            if (this.signupData.account_type == 'company'){
                                this.navigateToStep('directors_details');
                            }
                            else{
                                this.navigateToStep('id_verification');
                            }
                            break;
                        default:
                            this.navigateToStep('address');
                            break;
                    }
                break;

                case 'abn':
                    switch(direction){
                        case 'next':
                            this.navigateToStep('payment_method');
                            break;
                        case 'prev':
                            this.navigateToStep('address');
                            break;
                        default:
                            this.navigateToStep('abn');
                            break;
                    }
                break;

                case 'payment_method':
                    switch(direction){
                        case 'next':

                            switch(this.signupData.payment_method){
                                case 'auto_debit_credit_card':
                                    this.navigateToStep('payment_method_auto_debit_credit_card');
                                    break;
                                case 'direct_debit':
                                    this.navigateToStep('payment_method_direct_debit');
                                    break;
                                default:
                                    this.alert.info("Under Construction...");
                                    break;
                            }
                            break;

                        case 'prev':
                            this.navigateToStep('abn');
                            break;
                        default:
                            this.navigateToStep('payment_method');
                            break;
                    }
                break;

                case 'payment_method_auto_debit_credit_card':
                    switch(direction){
                        case 'next':
                            this.signUp();
                            break;
                        case 'prev':
                            this.navigateToStep('payment_method');
                            break;
                        default:
                            this.navigateToStep('payment_method_auto_debit_credit_card');
                            break;
                    }
                break;

                case 'payment_method_direct_debit':
                    switch(direction){
                        case 'next':
                            this.signUp();
                            break;
                        case 'prev':
                            this.navigateToStep('payment_method');
                            break;
                        default:
                            this.navigateToStep('payment_method_direct_debit');
                            break;
                    }
                break;

                case 'thank_you':
                    this.navigateToStep('thank_you');
                break;

            }
        }
        else{
            this.router.navigate(['/user/signup/auth_data']);
            this.stepAuthData();
        }
    }

    logData(){
        console.log(this.signupData);
    }

    private assignData(data){
        Object.assign(this.signupData, data);
        sessionStorage.setItem('signupData', JSON.stringify(this.signupData));
    }
}

@Component({
  selector: 'acknowledgement-dialog',
  templateUrl: 'acknowledgement-dialog.html',
})
export class SignupAcknowledgementDialog {

    constructor(public dialogRef: MatDialogRef<SignupAcknowledgementDialog>){}

}

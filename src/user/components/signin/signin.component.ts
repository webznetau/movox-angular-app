import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators } from '@angular/forms';
import {AlertService} from '../../../app/shared/services/alert.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {

    valid: boolean;

    constructor(
        private alert: AlertService
    ) { }
  
    loginForm : FormGroup = new FormGroup({        
        email: new FormControl('', [
            Validators.required,
            Validators.email
        ]),
        password: new FormControl('', [
            Validators.required
        ]),
        remember_me: new FormControl( false, [])
    });
    
    signin(): void{
        this.alert.info("Signin");
        this.valid = true;
    }
    
    invalid(): void{
        this.valid = false;
    }

  ngOnInit() {
  }

}

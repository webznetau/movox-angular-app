export class Credentials {
    uid: number;
    token: string;
}

export class UserData {
    email: string;
}

export class LoginData {
    email: string;
    password: string;
}

export class SignupData {
    email: string;
    first_name: string;
    last_name: string;
    password: string;
    password_repeat: string;
}
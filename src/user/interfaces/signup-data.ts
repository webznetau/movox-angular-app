export interface SignupData{
    address: SignupDataAddress;
    correspondence_address: SignupDataAddress;
    correspondence_address_same?: boolean;
    accept_customer_terms?: boolean;
    email?: string;
    password?: string;
    account_type?: string;
    name_title?: string;
    first_name?: string;
    last_name?: string;
    contact_number?: string;
    mobile_number?: string;
    date_of_birth?: string;
    abn?: string;
    abn_legal_name?: string;
    abn_entity_type?: string;
    abn_trading_as?: string;
    payment_method?: string;
    accept_payment_auto_debit_credit_card?: boolean;
    accept_payment_auto_debit_credit_card_privacy?: boolean;
    accept_payment_direct_debit?: boolean;
    accept_payment_direct_debit_privacy?: boolean;
    credit_card_holder_name?: string;
    credit_card_number?: string;
    credit_card_cvc?: string;
    credit_card_valid_to?: string;
    direct_debit_bank_name?: string;
    direct_debit_bsb?: string;
    direct_debit_account_number?: string;
    direct_debit_account_name?: string;
    directors?: SignupDataDirector[];
};

export interface SignupDataDirector{
    name_title?: string;
    first_name?: string;
    last_name?: string;
    mobile_number?: string;
    date_of_birth?: string;
}

export interface SignupDataAddress{
    po_box?: string;
    unit_number?: string;
    street_number?: string;
    street_name?: string;
    street_type?: string;
    suburb?: string;
    state?: string;
    postcode?: string;
}
//Ng
import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';

//Components
import {SignupComponent} from './components/signup/signup.component';
import {SigninComponent} from './components/signin/signin.component';

//Guards
import {LoggedInRouteGuard} from '../app/shared/guards/logged-in-route-guard';
import {LoggedOutRouteGuard} from '../app/shared/guards/logged-out-route-guard';

const routes: Routes = [
    {
        path: 'signin',
        component: SigninComponent
    },
    {
        path: 'signup/:step',
        component: SignupComponent,
    },
    {
        path: 'signup',
        component: SignupComponent,
    },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: [LoggedInRouteGuard, LoggedOutRouteGuard]
})
export class UserRoutingModule {}

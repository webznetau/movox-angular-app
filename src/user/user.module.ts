import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserRoutingModule} from './user-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {DateFormat} from '../app/core/date-format';
import {DateAdapter} from '@angular/material';

//Material
import {MatInputModule} from '@angular/material'
import {MatCardModule} from '@angular/material/card';
import {MatIconModule} from '@angular/material/icon';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatButtonModule} from '@angular/material/button';
import {MatSelectModule} from '@angular/material/select';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatProgressBarModule} from '@angular/material/progress-bar';

//Components
import {SignupComponent, SignupAcknowledgementDialog} from './components/signup/signup.component';
import {SigninComponent} from './components/signin/signin.component';
import {AddressFinderComponent} from '../app/shared/components/address-finder/address-finder.component';
import {AbnFinderComponent} from '../app/shared/components/abn-finder/abn-finder.component';
import {CreditCardFormComponent} from '../app/shared/components/credit-card-form/credit-card-form.component';
import {MatNativeDateModule} from '@angular/material';
import {MatDatepickerModule} from '@angular/material/datepicker';

@NgModule({
    imports: [
        CommonModule,
        UserRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        //Material
        MatInputModule,
        MatCardModule,
        MatFormFieldModule,
        MatIconModule,
        MatCheckboxModule,
        MatProgressSpinnerModule,
        MatButtonModule,
        MatSelectModule,
        MatAutocompleteModule,
        MatProgressBarModule,
        MatNativeDateModule,
        MatDatepickerModule
    ],
    declarations: [
        SignupComponent,
        SignupAcknowledgementDialog,
        SigninComponent,
        AddressFinderComponent,
        AbnFinderComponent,
        CreditCardFormComponent
    ],
    entryComponents: [SignupAcknowledgementDialog],
    providers: [
      { provide: DateAdapter, useClass: DateFormat }
    ]
})
export class UserModule { 
    constructor(private dateAdapter:DateAdapter<Date>) {
        dateAdapter.setLocale('en-au');
    }
}
